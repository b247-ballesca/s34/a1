let http = require("http");

http.createServer(function (request, response) {

	if(request.url == "/items" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Data Retrieved from the database!')
	}

	if(request.url == "/items" && request.method == "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Data to be sent to database!')
	}
}).listen(4001);

console.log('Server Running')